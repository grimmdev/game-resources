function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}

defer(function () {
    console.log("External JS Loaded.");
    if ( $( "div.game-header" ).length ) {

    } else {
        //$('body').prepend('<div class="alert alert-primary" role="alert">External JS Loaded.</div>');
        $('body').prepend('<div class="game-header">' + '<div class="btn-close-view"><i class="ion-arrow-left-b"></i> CLOSE</div>' + '</div>');
            
        $("div.btn-close-view").click(function() {
            //alert('Response');
            window.location.href = 'uniwebview://close';
        });
    }
});